FROM python:3 AS base
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt && rm /code/requirements.txt

FROM base as dev
COPY requirements-dev.txt /code/
RUN pip install -r requirements-dev.txt && rm /code/requirements-dev.txt
