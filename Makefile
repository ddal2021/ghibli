.PHONY : setup-env dev

dev:
	pip install -r requirements.txt -r requirements-dev.txt
	pre-commit install

setup-env:
	cp .env.template .env
