from django.apps import AppConfig


class MoviesConfig(AppConfig):
    name = "ghibli.movies"

    def ready(self):
        # Run once at the beginning to make sure
        # we don't wait for a minute to populate the
        # database with initial data.
        from .tasks import update_info

        update_info.apply()
