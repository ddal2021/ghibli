from __future__ import absolute_import, unicode_literals

import logging
from datetime import datetime

from celery import shared_task
from django.core.cache import cache

from ghibli.movies.scrapper import save_movies, save_people
from ghibli.movies import settings as local_settings

logger = logging.getLogger(__name__)


@shared_task
def update_info():
    save_movies()
    save_people()
    cache.set(local_settings.CACHE_KEY, datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    logger.info("Movies and characters updated successfully")
