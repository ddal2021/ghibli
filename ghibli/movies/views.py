from django.core.cache import cache
from django.views.generic import ListView

from ghibli.movies.models import Movie
from ghibli.movies import settings as local_settings


class MoviesView(ListView):
    model = Movie
    queryset = Movie.objects.prefetch_related("character_set")
    template_name = "movie_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["latest_updated_at"] = cache.get(local_settings.CACHE_KEY)
        return context
