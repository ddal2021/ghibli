from dataclasses import dataclass, field
from typing import Dict, Any

import requests
from requests import RequestException

from ghibli.movies import settings as local_settings
from ghibli.movies.exceptions import FetchException
from ghibli.movies.models import Movie, Character


@dataclass
class Fetcher:
    """Thin wrapper around requests to keep things DRY"""

    base_url: str
    path: str
    params: Dict[str, str] = field(default_factory=dict)

    @property
    def full_path(self) -> str:
        return f"{self.base_url}{self.path}"

    def fetch(self, method: str = "get") -> Dict[Any, Any]:
        try:
            response = requests.request(
                method=method, url=self.full_path, params=self.params
            )
        except RequestException as e:
            raise FetchException(f"Fetching failed: {e}")
        try:
            return response.json()
        except ValueError as e:
            raise FetchException(f"Invalid response: {e}")


def save_movies():
    """Save films to the database"""
    results = Fetcher(
        base_url=local_settings.GHIBLI_ROOT_URL,
        path="/films",
        params={"fields": "id,title"},
    ).fetch()

    for movie in results:
        Movie.objects.update_or_create(
            defaults={"guid": movie["id"], "title": movie["title"]}, guid=movie["id"]
        )


def save_people():
    """Save characters to the database"""
    results = Fetcher(
        base_url=local_settings.GHIBLI_ROOT_URL,
        path="/people",
        params={"fields": "name,films"},
    ).fetch()

    for character in results:
        movies_guids = [f.rsplit("/", 1)[1] for f in character["films"]]
        movies = Movie.objects.filter(guid__in=movies_guids)
        c, _ = Character.objects.get_or_create(name=character["name"])
        c.movies.set(movies)
