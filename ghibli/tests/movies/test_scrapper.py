from django.test import TestCase

import responses
from requests import RequestException

from ghibli.movies.exceptions import FetchException
from ghibli.movies.models import Movie, Character
from ghibli.movies.scrapper import Fetcher, save_movies, save_people
from ghibli.movies import settings


class FetcherTestCase(TestCase):
    """Test cases for the scrapper module"""

    def setUp(self):
        self.fake_base = "http://twitter.com/"
        self.fake_path = "api/1/foobar"
        self.fake_url = f"{self.fake_base}{self.fake_path}"

    def tearDown(self):
        responses.reset()

    @responses.activate
    def test_fetcher_request_exception(self):
        """When RequestException is raised"""
        responses.add(responses.GET, self.fake_url, body=RequestException(""))

        with self.assertRaises(FetchException) as e:
            Fetcher(self.fake_base, self.fake_path).fetch()
        self.assertIn("Fetching failed", str(e.exception))

    @responses.activate
    def test_fetcher_invalid_json(self):
        """When ValueError is raised"""
        responses.add(responses.GET, self.fake_url, body="foo")
        with self.assertRaises(FetchException) as e:
            Fetcher(self.fake_base, self.fake_path).fetch()
        self.assertIn("Invalid response", str(e.exception))

    @responses.activate
    def test_fetcher_success(self):
        """Happy path :)"""
        responses.add(responses.GET, self.fake_url, json={"hello": "world"})
        response = Fetcher(self.fake_base, self.fake_path).fetch()
        self.assertDictEqual(response, {"hello": "world"})


class SaveMoviesTestCase(TestCase):
    def setUp(self):
        self.fake_uuid_1 = "82ff116c-ecba-444f-beb4-0bf8b1eea8ab"
        self.fake_uuid_2 = "2857c928-2ed5-47d2-8b3d-f67dd00f8856"

    def tearDown(self):
        responses.reset()

    @responses.activate
    def test_save_movies(self):
        """Save movies to the database"""
        responses.add(
            responses.GET,
            f"{settings.GHIBLI_ROOT_URL}/films?fields=id,title",
            json=[
                {"id": self.fake_uuid_1, "title": "movie1"},
                {"id": self.fake_uuid_2, "title": "movie2"},
            ],
        )
        save_movies()
        movies = Movie.objects.all()
        self.assertEqual(len(movies), 2)
        self.assertEqual(movies[0].title, "movie1")
        self.assertEqual(str(movies[0].guid), self.fake_uuid_1)
        self.assertEqual(movies[1].title, "movie2")
        self.assertEqual(str(movies[1].guid), self.fake_uuid_2)

    @responses.activate
    def test_update_movies(self):
        """Update existing records"""
        responses.add(
            responses.GET,
            f"{settings.GHIBLI_ROOT_URL}/films?fields=id,title",
            json=[
                {"id": self.fake_uuid_1, "title": "movie1.0"},
                {"id": self.fake_uuid_2, "title": "movie2"},
            ],
        )
        save_movies()
        movies = Movie.objects.all()
        self.assertEqual(len(movies), 2)
        self.assertEqual(movies[0].title, "movie1.0")
        self.assertEqual(str(movies[0].guid), self.fake_uuid_1)
        self.assertEqual(movies[1].title, "movie2")
        self.assertEqual(str(movies[1].guid), self.fake_uuid_2)


class SavePeopleTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.fake_uuid_1 = "82ff116c-ecba-444f-beb4-0bf8b1eea8ab"
        cls.fake_uuid_2 = "2857c928-2ed5-47d2-8b3d-f67dd00f8856"
        cls.fake_uuid_3 = "7b46f3d4-7916-4373-8577-550cdeca83d9"
        cls.m1 = Movie(guid=cls.fake_uuid_1, title="movie1")
        cls.m2 = Movie(guid=cls.fake_uuid_2, title="movie2")
        cls.m3 = Movie(guid=cls.fake_uuid_3, title="movie3")
        Movie.objects.bulk_create([cls.m1, cls.m2, cls.m3])

    def tearDown(self):
        responses.reset()

    @responses.activate
    def test_save_characters(self):
        """Save characters and associate them with movies"""
        responses.add(
            responses.GET,
            f"{settings.GHIBLI_ROOT_URL}/people?fields=name,films",
            json=[
                {
                    "name": "Totoro",
                    "films": [
                        f"fake_url/{self.fake_uuid_1}",
                        f"fake_url/{self.fake_uuid_2}",
                    ],
                },
                {"name": "Chihiro", "films": [f"fake_url/{self.fake_uuid_1}"]},
            ],
        )
        save_people()
        people = Character.objects.all()
        self.assertEqual(len(people), 2)
        self.assertEqual(people[0].name, "Totoro")
        self.assertEqual(list(people[0].movies.all()), [self.m1, self.m2])
        self.assertEqual(people[1].name, "Chihiro")
        self.assertEqual(list(people[1].movies.all()), [self.m1])

    @responses.activate
    def test_update_characters(self):
        """Add another movie"""
        responses.add(
            responses.GET,
            f"{settings.GHIBLI_ROOT_URL}/people?fields=name,films",
            json=[
                {
                    "name": "Totoro",
                    "films": [
                        f"fake_url/{self.fake_uuid_1}",
                        f"fake_url/{self.fake_uuid_2}",
                    ],
                },
                {
                    "name": "Chihiro",
                    "films": [
                        f"fake_url/{self.fake_uuid_1}",
                        f"fake_url/{self.fake_uuid_3}",
                    ],
                },
            ],
        )
        save_people()
        people = Character.objects.all()
        self.assertEqual(len(people), 2)
        self.assertEqual(people[0].name, "Totoro")
        self.assertEqual(list(people[0].movies.all()), [self.m1, self.m2])
        self.assertEqual(people[1].name, "Chihiro")
        self.assertEqual(list(people[1].movies.all()), [self.m1, self.m3])
