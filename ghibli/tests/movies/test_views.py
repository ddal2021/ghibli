from django.test import TestCase, override_settings
from django.urls import reverse

from ghibli.movies.models import Movie, Character


@override_settings(
    CACHES={"default": {"BACKEND": "django.core.cache.backends.dummy.DummyCache"}}
)
class MovieViewTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        fake_uuid_1 = "82ff116c-ecba-444f-beb4-0bf8b1eea8ab"
        fake_uuid_2 = "2857c928-2ed5-47d2-8b3d-f67dd00f8856"
        m1 = Movie(title="Movie1", guid=fake_uuid_1)
        m2 = Movie(title="Movie2", guid=fake_uuid_2)
        cls.movie_list = [m1, m2]
        Movie.objects.bulk_create(cls.movie_list)
        c1 = Character(name="Char1")
        c2 = Character(name="Char2")
        cls.character_list = [c1, c2]
        Character.objects.bulk_create(cls.character_list)
        c1.movies.set([m1])
        c2.movies.set([m1, m2])

    def test_movies_list(self):
        """Test that MovieListView renders all the movies and characters"""
        # 1 for the Movies + 1 for the characters (prefetch related)
        with self.assertNumQueries(2):
            response = self.client.get(reverse("movies-list"))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Last updated at")
        self.assertQuerysetEqual(
            response.context["object_list"],
            ["<Movie: Movie1>", "<Movie: Movie2>"],
            ordered=False,
        )
        self.assertQuerysetEqual(
            response.context["object_list"][0].character_set.all(),
            ["<Character: Char1>", "<Character: Char2>"],
            ordered=False,
        )
        self.assertQuerysetEqual(
            response.context["object_list"][1].character_set.all(),
            ["<Character: Char2>"],
            ordered=False,
        )
