"""ghibli URL Configuration"""
from django.urls import path

from .movies.views import MoviesView

urlpatterns = [path(route="movies/", view=MoviesView.as_view(), name="movies-list")]
