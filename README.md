Project Ghibli
===
Show studio Ghibli's movies and characters

Development
---
The tech that was used is:

  * Django for the web parts
  * Celery for running periodic tasks - Update the movies info every minute
  * Postegresql as a RDBMS
  * Redis as a cache and as a celery broker
  * Docker and docker-compose for deploying locally

Run the following command to setup the dev environment. Needs to be run once.

```shell
$> make dev
```

Running
---
Requirements: docker-compose and docker.
Deploy with:

```shell
$> make setup-env
$> docker-compose up -d web
```
The first time it will take some time because of the docker image building/downloading.
Once it's done open http://localhost:8000/movies in a browser.

To run the tests:

```shell
$> docker-compose build test
$> docker-compose run --rm test
```

Notes
---
Due to time constraints I didn't add a production deployment method/configuration.
If I had the time I would use, for example nginx + gunicorn.
